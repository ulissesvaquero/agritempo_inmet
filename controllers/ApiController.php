<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

/**
 *
 * @author ulissesvaquero
 *        
 */
class ApiController extends Controller {
	public function behaviors() {
		return [ 
				'corsFilter' => [ 
						'class' => \yii\filters\Cors::className () 
				] 
		];
	}
	
	
	public function actionExecSql($sql)
	{
		$result = \Yii::$app->db2->createCommand ($sql)->queryAll();
		
		var_dump($result);
	}
	
	
	/**
	 * Pega dados do modelo
	 * http://www.inmet.gov.br/agritempo_inmet/web/index.php?r=api/get-dados&dataInicial=2017-08-01&dataFinal=2017-08-04&geocode=1200104
	 * @param string  $dataInicial Formato Y-m-d H:i:s
	 * @param string  $dataFim     	
	 * @param integer $geocode        	
	 * @param integer $modelo_id  
	 * @resource r=api/get-dados&dataInicial=2017-08-01&dataFinal=2017-08-02&geocode=1200104      	
	 * @return json|string
	 */
	public function actionGetDados($dataInicial,$dataFinal,$geocode) {
		
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		$dataInicial= \DateTime::createFromFormat('Y-m-d', $dataInicial);
		
		$dataFinal= \DateTime::createFromFormat('Y-m-d', $dataFinal);
		
		if(!$dataInicial)
		{
			return ['message' => 'Data Inicial Inválida'];
		}
		
		if(!$dataFinal)
		{
			return ['message' => 'Data Final Inválida'];
		}
		
		$sql = "
				SELECT 
						geocode, 
						data, 
						t_min, 
						t_max, 
						u_min, 
						u_max, 
						nebulosidade, 
						precipitacao, 
       					vento_vel, 
						vento_dir, 
						vento_ori, 
						pressao, 
						tot_prec
  				FROM 
					modelo.vw_dados_modelos 
				where 
					data BETWEEN '{$dataInicial->format('Y-m-d')}' AND '{$dataFinal->format('Y-m-d')}'
				and
					geocode = '$geocode'
		";
		
		$result = \Yii::$app->db2->createCommand($sql)->queryAll ();
		
		return $result;		
	}
}
