<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\DadosEstacao;
use app\models\DadosMunicipio;
use yii\web\JsonResponseFormatter;
use yii\helpers\VarDumper;
use app\repositories\MunicipioRepository;
use app\repositories\EstacaoRepository;
use app\models\DadoHistoricoMensal;
use app\repositories\DadoHistoricoMensalRepository;
use yii\helpers\ArrayHelper;
use app\models\Estacao;
/**
 * @author ulissesvaquero
 *
 */
class SiteController extends Controller
{
	
	public function behaviors()
	{
		return [
				'corsFilter' => [
						'class' => \yii\filters\Cors::className(),
				],
		];
	}
	
	
	
	protected function getMesLabel($mes)
	{
		$arrMes = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
				'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
	
	
		return $arrMes[$mes-1];
	
	}
	
	
	protected function getMesAnoLabel($mes,$ano)
	{
		$arrMes = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
				'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
		
		
		return $arrMes[$mes-1].'/'.$ano;
		
	}
	
	protected function getMeses($mesFinal,$mesInicial=0)
	{
		$arrMes = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 
				   'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
		
		$arrToReturn = [];
		for($i=$mesInicial;$i<$mesFinal;$i++)
		{
			$arrToReturn[] = $arrMes[$i];
		}
		
		krsort($arrToReturn);
		
		return array_values($arrToReturn);		
	}
	
	public function getMediaAno($estacao_id,$mes)
	{
		$estacao = Estacao::findOne($estacao_id);
		$path = \Yii::getAlias('@arquivo/'.'historico_prec_acumulada.csv');
		$handle = fopen($path, "r");
		
		//Posicao de atributos
		$mes = $mes + 2;
		
		if ($handle) {
			$contador = 0;
			while (($line = fgets($handle)) !== false) {
				$arrExplode = explode(';', $line);
				$codigoEstacao = (int) $arrExplode[0];
				$qtdAtributoExcel = count($arrExplode);
				
				if($codigoEstacao == $estacao->codigo)
				{
					//Precipitacao Acumulada no ano
					$precAcumuladaAno = $arrExplode[$mes];
					$precAcumuladaAno = str_replace('.', '', $precAcumuladaAno);
					$precAcumuladaAno = str_replace(',', '.', $precAcumuladaAno);
					$precAcumuladaAno = floatval($precAcumuladaAno);
					
					return $precAcumuladaAno;
				}
			}
		}
		
		return false;
	}
	
	
	
	/**
	 * http://localhost/agritempo_inmet/web/index.php?r=site/dado-historico&estacao_id=1
	 * 
	 * PRIMEIRA BARRA
	 * ano atual
	 * ano anterior
	 * 
	 * @todo fazer uma verfica��o breve se tem informa��es sobre a esta��o.
	 */
	public function actionDadoHistorico($estacao_id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$arrDadoMes = [];
		
		/**
		 * Pego os ultimos 12 meses
		 */
		$format = 'm/y';
		//Repons�vel por controlar e informar se a esta��o est� com todos os dados zerados.
		$tudoZero = false;
		$media5Anos = false;
		$dadosAtual = [];
		$dadosAnterior = [];
		$dadosMedia = [];
		$arrMesAno = [];
		$qtdDivisao = (int) DadoHistoricoMensalRepository::find()->distinct(true)->select('ano')->count();
		
		//Recupero todos os meses do ano
		$arrNumeroMes = range(1,12);
		$ano = (int) substr(date('Y'),2);
		
		foreach ($arrNumeroMes as $numeroMes)
		{
			$mes = $numeroMes;
			
			//Historico de chuva desse ano atual
			$historicoAnoAtual = DadoHistoricoMensal::find()
				->where(['estacao_id' => $estacao_id, 'mes' => $mes, 'ano' => $ano])
				->asArray(true)
				->one();
				
			//Mesmo hist�rico no ano anterior
			$historicoAnoAnterior = DadoHistoricoMensal::find()
				->where(['estacao_id' => $estacao_id, 'mes' => $mes, 'ano' => $ano-1])
				->asArray(true)
				->one();
				
			if(!$mediaAno = $this->getMediaAno($estacao_id,$mes))
			{
				//Média dado histórico
				$mediaMes = DadoHistoricoMensal::find()
					->select("(sum(precipitacao) / $qtdDivisao) as media")
					->where(['estacao_id' => $estacao_id, 'mes' => $mes])
					->asArray(true)
					->one();
				
				$mediaAno= ArrayHelper::getValue($mediaMes, 'media',0);
			}else
			{
				$media5Anos = true;
			}
			
			$precipitacaoAnoAtual = ArrayHelper::getValue($historicoAnoAtual, 'precipitacao',0);
			$precipitacaoAnoAnterior = ArrayHelper::getValue($historicoAnoAnterior, 'precipitacao',0);
			//$mediaMesTodosAnos = ArrayHelper::getValue($mediaMes, 'media',0);
				
			if($precipitacaoAnoAtual == 0 && $precipitacaoAnoAnterior == 0  && $mediaAno == 0)
			{
				$tudoZero = true;
			}
			
			$dadosAtual[] = $precipitacaoAnoAtual;
			$dadosAnterior[] = $precipitacaoAnoAnterior;
			$dadosMedia[] = $mediaAno;
			$arrMesAno[] = $this->getMesLabel($mes, $ano);
		}
			
			
		//krsort($dadosAtual);
		//krsort($dadosAnterior);
		//krsort($dadosMedia);
		//krsort($arrMesAno);
			
			
		return [
				'dados_atual' => ArrayHelper::getColumn($dadosAtual, function ($element) {
				return round(floatval($element),1);
				},false),
				'dados_anterior' => ArrayHelper::getColumn($dadosAnterior, function ($element) {
				return round(floatval($element),1);
				},false),
				'media' => ArrayHelper::getColumn($dadosMedia, function ($element) {
				return round(floatval($element),1);
				},false),
				'meses' => array_values($arrMesAno),
				'estacao' => EstacaoRepository::findOne($estacao_id),
				'tudoZero' => $tudoZero,
				'media5Anos' => $media5Anos
			];
		
		
		
		
		exit;
		
		
		
		
		
		
		for ($t = 0; $t < 12; $t++) {
			$dateString = date($format, strtotime( date( 'Y-m-01' )." -$t months"));
			list($mes,$ano) = explode('/',$dateString) ;
			$mes = (int) $mes;
			$ano = (int) $ano;
			
			//Historico de chuva desse ano atual
			$historicoAnoAtual = DadoHistoricoMensal::find()
								->where(['estacao_id' => $estacao_id, 'mes' => $mes, 'ano' => $ano])
								->asArray(true)
								->one();
			
			//Mesmo hist�rico no ano anterior
			$historicoAnoAnterior = DadoHistoricoMensal::find()
								->where(['estacao_id' => $estacao_id, 'mes' => $mes, 'ano' => $ano-1])
								->asArray(true)
								->one();
			
			//Média dado histórico
			$mediaMes = DadoHistoricoMensal::find()
								->select("(sum(precipitacao) / $qtdDivisao) as media")
								->where(['estacao_id' => $estacao_id, 'mes' => $mes])
								->asArray(true)
								->one();
			
			$precipitacaoAnoAtual = ArrayHelper::getValue($historicoAnoAtual, 'precipitacao',0);
			$precipitacaoAnoAnterior = ArrayHelper::getValue($historicoAnoAnterior, 'precipitacao',0);
			$mediaMesTodosAnos = ArrayHelper::getValue($mediaMes, 'media',0);
			
			if($precipitacaoAnoAtual == 0 && $precipitacaoAnoAnterior == 0  && $mediaMesTodosAnos == 0)
			{
				$tudoZero = true;
			}
								
			$dadosAtual[] = $precipitacaoAnoAtual;
			$dadosAnterior[] = $precipitacaoAnoAnterior;
			$dadosMedia[] = $mediaMesTodosAnos;
			$arrMesAno[] = $this->getMesAnoLabel($mes, $ano);
		}
		
		
		krsort($dadosAtual);
		krsort($dadosAnterior);
		krsort($dadosMedia);
		krsort($arrMesAno);
		
		
		return [
				'dados_atual' => ArrayHelper::getColumn($dadosAtual, function ($element) {
         			return round(floatval($element),1);
     			},false),
				'dados_anterior' => ArrayHelper::getColumn($dadosAnterior, function ($element) {
         			return round(floatval($element),1);
     			},false),
				'media' => ArrayHelper::getColumn($dadosMedia, function ($element) {
         			return round(floatval($element),1);
     			},false),
				'meses' => array_values($arrMesAno),
				'estacao' => EstacaoRepository::findOne($estacao_id),
				'tudoZero' => $tudoZero
		];
		
		return $dadoHistorico;
	}
	
	/**
	 * http://localhost/agritempo_inmet/web/index.php?r=site/municipio&term=brasilia
	 * @param string $term
	 * @return string
	 */
	public function actionMunicipio($term)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result = file_get_contents('http://www.inmet.gov.br/portal/index.php?r=municipio/sugestMunicipio&term='.urlencode($term));
		return json_decode($result);
	}
	
	/**
	 * http://localhost/agritempo_inmet/web/index.php?r=site/previsao&code=5300108
	 * @param integer $code
	 * @return string
	 */
	public function actionPrevisao($code)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		/**
		 * http://www.inmet.gov.br/portal/index.php?r=prevmet/previsaoWebservice/getJsonPrevisaoDiariaPorCidade&code=5300108 
		 * @var Ambiguous $result
		 */
		//URL ANTIGA
		//$result = file_get_contents('http://www.inmet.gov.br/portal/index.php?r=tempo2/previsaoDeTempo&code='.$code);
		
		$result = file_get_contents('http://www.inmet.gov.br/portal/index.php?r=prevmet/previsaoWebservice/getJsonPrevisaoDiariaPorCidade&code='.$code);
		
		$result = json_decode($result,true);
		$arrToReturn = [];
		$municipio = MunicipioRepository::findOne(['geocode' => $code]);
		$arrEstacaoProxima = $this->getEstacaoProxima($municipio);
		
		if(isset($result[$code]))
		{
			$dadoEstacao = $result[$code];
			$hora = date('H');
			$arrManha = range(6,12);
			$arrTarde = range(13,18);
				
				
			foreach ($dadoEstacao as $data => $dado)
			{
				$toReturn = [];
				$date = \DateTime::createFromFormat('d-m-Y', $data);
				$pos = 'noite';
				//MANHA
				if(in_array($hora, $arrManha))
				{
					$pos = 'manha';
					//TARDE
				}elseif(in_array($hora, $arrTarde))
				{
					$pos = 'tarde';
				}
		
				//Verifico se � o primeiro ou segundo pois esses preciso dos turnos de manha, tarde , noite
				if(count($arrToReturn) < 2)
				{
					$toReturn = [
							'data' => $date->format('d/m'),
							'm' => $dado['manha'],
							't' => $dado['tarde'],
							'n' => $dado['noite'],
							'c' => $dado[$pos]
					];
						
				}else
				{
					$toReturn = array_merge(['data' => $date->format('d/m')],$dado);
				}
		
				$arrToReturn[] = $toReturn;
			}
		}
		
		
		$arrToReturn = array_merge(['estacao_proxima' => $arrEstacaoProxima , 'geocode' => $code , 'municipio' => $municipio],['resultado' => $arrToReturn]);
		
		return $arrToReturn;
	}
	
	/**
	 * M�todo respons�vel por retornar dados de uma esta��o.
	 * endpoint = http://localhost/agromet/web/index.php?r=site%2Fget-data-estacao&lat=-11.929&lon=-61.995998
	 * Definir qual o hor�rio utilizar na hora do retorno.
	 * To search by kilometers instead of miles, replace 3959 with 6371
	 * DE acordo com a OMM
	 * Agrupar um array com esta��es e horarios.
	 */
	public function getEstacaoProxima($municipio)
	{
		$distancia=250;
		
		$lat = $municipio->lat;
		$lon = $municipio->lon;
		
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		$sql = "SELECT 
					*, 
					( 6371 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lon ) - radians($lon) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distancia 
				FROM 
					estacao
				WHERE
					is_ativo = 1
				HAVING 
					distancia < $distancia 
				ORDER BY 
					distancia
				LIMIT 
					5";
		
		return \Yii::$app->db->createCommand($sql)->queryAll();
	}
	
	
	/**
	 * M�todo respons�vel por retornar dados de um municipio.
	 * @param unknown $codMunicipio
	 * http://localhost/agromet/web/index.php?r=site/previsao&code=5300108	
	 */
	public function actionGetDataMunicipio($cod)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return DadosMunicipio::findOne(['cod_municipio' => $cod]);
	}
}
