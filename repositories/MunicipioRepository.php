<?php

namespace app\repositories;

use Yii;
use app\models\Municipio;


class MunicipioRepository extends Municipio 
{
   public function formata()
   {
   		$this->lat = (float) str_replace(',','.', $this->lat);
   		$this->lon = (float) str_replace(',','.', $this->lon);
   }
   
   
   
}
