<?php

namespace app\commands;

use yii\console\Controller;
use app\repositories\EstacaoRepository;
use app\repositories\DadoHistoricoMensalRepository;
use app\repositories\MunicipioRepository;
use app\models\Estacao;


/* @var $estacao app\repositories\EstacaoRepository */

class ImportadorController extends Controller
{
	
	/**
	 * Revisa histórico precipitação
	 */
	public function actionRevisaHistoricoPrecipitacao($estacao_id)
	{
		$estacao = Estacao::findOne($estacao_id);
		$path = \Yii::getAlias('@arquivo/'.'historico_prec_acumulada.csv');
		$handle = fopen($path, "r");
		if ($handle) {
			$contador = 0;
			while (($line = fgets($handle)) !== false) {
				$arrExplode = explode(';', $line);
				$codigoEstacao = (int) $arrExplode[0];
				$qtdAtributoExcel = count($arrExplode);
				
				if($codigoEstacao == $estacao->codigo)
				{
					//Precipitacao Acumulada no ano
					$precAcumuladaAno = end($arrExplode);
					$precAcumuladaAno = str_replace('.', '', $precAcumuladaAno);
					$precAcumuladaAno = str_replace(',', '.', $precAcumuladaAno);
					$precAcumuladaAno = floatval($precAcumuladaAno);
					
					return $precAcumuladaAno;
				}
			}
		}
		
		return false;
	}
	
	
	
	public function actionRevisaEstacao()
	{
		$arrFileName = [
			\Yii::getAlias('@arquivo/'.'estacao_valida_final_aut.csv'),
			\Yii::getAlias('@arquivo/'.'estacao_valida_final_conv.csv')
		];
		
		//INATIVA TODOS
		EstacaoRepository::updateAll(['is_ativo' => 0]);
		
		
		foreach ($arrFileName as $path)
		{
			$handle = fopen($path, "r");
			if ($handle) {
				$contador = 0;
				while (($line = fgets($handle)) !== false) {
					$arrExplode = explode(' - ', $line);
					if(count($arrExplode) > 1)
					{
						$cod = $arrExplode[0];
						EstacaoRepository::updateAll(['is_ativo' => 1],['codigo' => $cod]);
					}
				}
			}
		}
	}
	
	
	
	
	/**
	 * Seta as estacoes operantes
	 */
	public function actionRotinaAtiva()
	{
		$fileName = 'estacao_operante.txt';
		$path = \Yii::getAlias('@arquivo/'.$fileName);
		$handle = fopen($path, "r");
		if ($handle) {
			$contador = 0;
			while (($line = fgets($handle)) !== false) {
				$arrExplode = explode(' - ', $line);
				if(count($arrExplode) > 1)
				{
					$codEstacao = (int)$arrExplode[0];
					$estacao = EstacaoRepository::findOne(['codigo' => $codEstacao]);
					$estacao->is_ativo = 1;
					$estacao->save();
				}
			}
		}
	}
	
	
	
	/**
	 * Rotina atualiza dados.
	 */
	public function actionAtualizaBase()
	{
		$pathDir = \Yii::getAlias('@arquivo/rotina');
		$arrFiles = scandir($pathDir);
		
		foreach ($arrFiles as $file)
		{
			if($file == '.' || $file == '..')continue;
			
			/*
			 * Se retornar A , automatica C , convencional
			 */
			$tipoEstacao = substr($file, 0,1);
			
			$this->leArquivoAtualizaDados($pathDir.'/'.$file);
			
			/*if($tipoEstacao == 'C')
			{
				$this->leArquivoAtualizaDados($pathDir.'/'.$file);
			}*/
		}
		
	}
	
	
	/**
	 * MAPEAMENTO DO ARQUIVO
	 * POSICAO , VALOR
	 * 0 = A108 - CRUZEIRO DO SUL
	 * 1 = AC
	 * 2 = LATITUDE
	 * 3 = LONGITUDE
	 * 4 = ALTITUDE
	 * 5 = DATA dd/mm/YYYY
	 * 6 = PRECIPITACAO
	 * @param unknown $pathFile
	 */
	public function leArquivoAtualizaDados($pathFile)
	{
		$contador = 0;
		$handle = fopen($pathFile, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				
				//Ignora a primeira linha do arquivo.
				$contador++;
				if($contador == 1)continue;
				$dados = explode(';',$line);
				
				foreach ($dados as $key => $d)
				{
					$dados[$key] = trim($d);
				}
				
				//Extrair o codigo da estação.
				$codNomeEstacao = $dados[0];
				$explodeCodNomeEstacao =  explode(' - ',$codNomeEstacao);
				
				
				if($explodeCodNomeEstacao)
				{
					$codigoEstacao = $explodeCodNomeEstacao[0];
					$nomeEstacao = $explodeCodNomeEstacao[1];
					
					//Procuro a estação pelo codigo.
					$estacao = EstacaoRepository::findOne(['codigo' => $codigoEstacao]);
					
					if($estacao)
					{
						$explodeDadoAno = explode('/',$dados[5]);
						
						//Tem planilha com coluna e valor trocado.
						if(!$explodeDadoAno)continue;
						$mes = (int) $explodeDadoAno[1];
						$ano = (int) substr($explodeDadoAno[2],2,2);
						$prec = (float) str_replace(',', '.', $dados[6]);
						
						//Se não tiver esse dado eu salvo, se tiver eu atualizo.
						if(!$dadoHistoricoMensal = DadoHistoricoMensalRepository::find()->where(['ano' => $ano, 'mes' => $mes, 'estacao_id' => $estacao->id])->one())
						{
							$dadoHistoricoMensal = new DadoHistoricoMensalRepository();
							$dadoHistoricoMensal->ano = $ano;
							$dadoHistoricoMensal->mes = $mes;
							$dadoHistoricoMensal->estacao_id = $estacao->id;
							$dadoHistoricoMensal->precipitacao = $prec;
							$dadoHistoricoMensal->save();
						}else {
							$dadoHistoricoMensal->precipitacao = $prec;
							$dadoHistoricoMensal->save();
						}
					}
				}
			}
			fclose($handle);
			
			//Faço a exclusão do arquivo
			unlink($pathFile);
		} else {
			// error opening the file.
		}
	}
	
	
	
	public function actionMunicipio()
	{
		$fileName = 'municipios.csv';
		$path = \Yii::getAlias('@arquivo/'.$fileName);
		$contadorLinha = 0;
		$handle = fopen($path, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				$contadorLinha++;
				if($contadorLinha == 1)continue;
				$dados = explode(';',$line);
		
				if($dados && count($dados))
				{
					$geocode = str_replace('"', '', $dados[0]);
					if(!MunicipioRepository::find()->where(['geocode' =>$geocode])->count())
					{
						$municipio = new MunicipioRepository();
						$municipio->geocode = $geocode;
						$municipio->nome = str_replace('"', '', $dados[1]);
						$municipio->uf = str_replace('"', '', $dados[2]);
						$municipio->lat = str_replace('"', '', $dados[3]);
						$municipio->lon = 	str_replace('"', '', $dados[4]);
						$municipio->formata();
						$municipio->save();
					}
				}
			}
			fclose($handle);
		} else {
			// error opening the file.
		}
	}
	
	
	
	/**
	 * Para relatório o sistema deve auto completar os meses não importados para a base.
	 */
	public function actionFormata()
	{
		$arrEstacao = EstacaoRepository::find()->all();
		$arrMes = range(1,12);
		$arrAno = range(13,16);
		
		foreach($arrEstacao as $estacao)
		{
			foreach ($arrAno as $ano)
			{
				foreach ($arrMes as $mes)
				{
					if(!DadoHistoricoMensalRepository::find()->where(['ano' => $ano, 'mes' => $mes, 'estacao_id' => $estacao->id])->count())
					{
						$dadoHistoricoMensal = new DadoHistoricoMensalRepository();
						$dadoHistoricoMensal->ano = $ano;
						$dadoHistoricoMensal->mes = $mes;
						$dadoHistoricoMensal->estacao_id = $estacao->id;
						$dadoHistoricoMensal->precipitacao = (float) 0;
						$dadoHistoricoMensal->save();
					}
				}
			}
		}
	}
	
	
	
	/**
	 * LÊ todos os CSVS e importa para base
	 */
	public function actionPrec()
	{
		exit;
		
		$pathFolder = \Yii::getAlias('@arquivo/precipitacao_mensal');
		$arrFiles = scandir($pathFolder);
		unset($arrFiles[0]);
		unset($arrFiles[1]);
		
		foreach ($arrFiles as $file)
		{
			$completoPath = $pathFolder.'/'.$file;
			//Verifico a extensão
			$pathInfo = pathinfo($completoPath);
			if($pathInfo['extension'] != 'csv')continue;
			$contadorLinha = 0;
			$handle = fopen($completoPath, "r");
			if ($handle) {
				while (($line = fgets($handle)) !== false) {
					$contadorLinha++;
					if($contadorLinha < 4)continue;
					$dados = explode(';',$line);
					
					if($dados && count($dados))
					{
						$explodeNomeCodigoEstacao = explode(' - ',$dados[0]);
						$codEstacao = $explodeNomeCodigoEstacao[0];
						$estacao = EstacaoRepository::findOne(['codigo' => $codEstacao]);
						
						if($estacao)
						{
							$explodeDadoAno = explode('/',$dados[5]);
							//Tem planilha com coluna e valor trocado.
							if(!$explodeDadoAno)continue;
							$mes = (int) $explodeDadoAno[0];
							$ano = (int) $explodeDadoAno[1];
							$prec = (float) str_replace(',', '.', $dados[6]);
							if(!DadoHistoricoMensalRepository::find()->where(['ano' => $ano, 'mes' => $mes, 'estacao_id' => $estacao->id])->count())
							{
								$dadoHistoricoMensal = new DadoHistoricoMensalRepository();
								$dadoHistoricoMensal->ano = $ano;
								$dadoHistoricoMensal->mes = $mes;
								$dadoHistoricoMensal->estacao_id = $estacao->id;
								$dadoHistoricoMensal->precipitacao = $prec;
								$dadoHistoricoMensal->save();
							}
						}
					}
			
				}
				fclose($handle);
			} else {
				// error opening the file.
			}
		}
	}
	
	
	public function actionEstacao()
	{
		$fileName = 'estacoes.csv';
		$path = \Yii::getAlias('@arquivo/'.$fileName);
		$contadorLinha = 0;
		$handle = fopen($path, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				$contadorLinha++;
				if($contadorLinha == 1)continue;
				$dados = explode(';',$line);
				
				if($dados && count($dados))
				{
					$estacao = new EstacaoRepository();
					$estacao->codigo =  $dados[0];
					$estacao->nome = $dados[3];
					$estacao->cidade = $dados[4];
					$estacao->uf = $dados[5];
					
					//TROCADO
					$estacao->lat = $dados[6];
					$estacao->lon = $dados[7];
					
					$estacao->lat = $dados[7];
					$estacao->lon = $dados[6];
					
					
					
					$estacao->hp = $dados[8];
					
					$estacao->formata();
					$estacao->save();
				}
				
			}
			fclose($handle);
		} else {
			// error opening the file.
		}
	}
	
	
	/**
	 * INSERIR DADOS MUNICIPIO
	 * LOAD DATA LOCAL INFILE '/home/ulissesvaquero/Desenvolvimento/PHP/agromet/web/arquivo/dados_municipio.csv'
	   INTO TABLE dados_municipio
	   FIELDS TERMINATED BY ','
	  (cod_municipio,lat,lon,data,t_2m,relhum_2m,tot_prec,clct,v10m,u10,v10,presmsl)
	 */
	
	/**
	 * INSERIR DADOS ESTACAO
	 * LOAD DATA LOCAL INFILE '/home/ulissesvaquero/Desenvolvimento/PHP/agromet/web/arquivo/dados_estacao.csv'
	   INTO TABLE dados_municipio
	   FIELDS TERMINATED BY ','
	  (cod_estacao,lat,lon,data,t_2m,relhum_2m,tot_prec,clct,v10m,u10,v10,presmsl)
	 */
	
	
	public function actionLoad()
	{
		$fileMunicipio = "/home/ulissesvaquero/Desenvolvimento/PHP/agromet/web/arquivo/dados_municipio.csv";
		exec("mysql -u root -p -e \"USE agromet;TRUNCATE dados_municipio;LOAD DATA LOCAL INFILE '" . $fileMunicipio . "' INTO TABLE dados_municipio FIELDS TERMINATED BY ','
	  			(cod_municipio,lat,lon,data,t_2m,relhum_2m,tot_prec,clct,v10m,u10,v10,presmsl);\"; ");
		
		$fileEstacao = "/home/ulissesvaquero/Desenvolvimento/PHP/agromet/web/arquivo/dados_estacao.csv";
		exec("mysql -u root -p -e \"USE agromet;TRUNCATE dados_estacao;LOAD DATA LOCAL INFILE '" . $fileEstacao . "' INTO TABLE dados_estacao FIELDS TERMINATED BY ','
	  			(cod_estacao,lat,lon,data,t_2m,relhum_2m,tot_prec,clct,v10m,u10,v10,presmsl);\"; ");
	}
	
	/**
	 * SELECT cod_estacao,id, ( 3959 * acos( cos( radians(-15.7797200) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-47.9297200) ) + sin( radians(-15.7797200) ) * sin( radians( lat ) ) ) ) AS distance FROM dados HAVING distance < 5 ORDER BY distance LIMIT 0 , 20;
	 * @param unknown $path
	 * Haversine
	 */
	
	public function actionTeste()
	{
		$path = \Yii::getAlias('@arquivo/'.'model_output.csv');
		$contadorLinha = 0;
		$handle = fopen($path, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				$contadorLinha++;
				if($contadorLinha == 1)continue;
				
				var_dump($line);exit;
				
				
				$dados = new \app\models\Dados();
				list($dados->cod_estacao,$dados->lat,$dados->lng) = explode(',',$line);
				$dados->save(false);
			}
			fclose($handle);
		} else {
			// error opening the file.
		}
	}
	
	
	
	
	protected function processaPrevisao($path)
	{
		$contadorLinha = 0;
		$handle = fopen($path, "r");
		if ($handle) {
		    while (($line = fgets($handle)) !== false) {
		    	$contadorLinha++;
		    	if($contadorLinha == 1)continue;
		    	$dados = new \app\models\Dados();
		    	list($dados->cod_estacao,$dados->lat,$dados->lng) = explode(',',$line);
		    	$dados->save(false);
		    }
		    fclose($handle);
		} else {
		    // error opening the file.
		} 
	}
	
	
	
	/**
	 * https://developers.google.com/maps/articles/phpsqlsearch_v3
	 * Finding Locations with MySQL
	  To find locations in your markers table that are within a certain radius distance of a given latitude/longitude, you can use a SELECT statement based on the Haversine formula. The Haversine formula is used generally for computing great-circle distances between two pairs of coordinates on a sphere. An in-depth mathemetical explanation is given by Wikipedia and a good discussion of the formula as it relates to programming is on Movable Type's site.
	  Here's the SQL statement that will find the closest 20 locations that are within a radius of 25 miles to the 37, -122 coordinate. It calculates the distance based on the latitude/longitude of that row and the target latitude/longitude, and then asks for only rows where the distance value is less than 25, orders the whole query by distance, and limits it to 20 results. To search by kilometers instead of miles, replace 3959 with 6371.
	 */
	protected function processaPrecipitacao($path)
	{
		$handle = fopen($path, "r");
		$contadorLinha = 0;
		
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				$contadorLinha++;
				/**
				 * I005,I005_3,I005_5,I005_10,I005_16,I005_30,I005_90
				 */
				//Pula as duas primeiras linhas
				if($contadorLinha == 1 || $contadorLinha == 2)continue;
				$arrInfo = explode(',', $line);
				list($codEstacao,
					 $data,
					 $lat,
					 $lng,
					 $prec,
					 $prec_3,
					 $prec_5,
					 $prec_10,
					 $prec_16,
					 $prec_30,
					 $prec_90
					) = $arrInfo;
				
					$arrDados = [
						'cod_estacao' => $codEstacao,
						'data' => $data,
						'lat'  => $lat,
						'lng' => $lng,
						'prec' => $prec,
						'prec_3' => $prec_3,
						'prec_5' => $prec_5,
						'prec_10' => $prec_10,
						'prec_16' => $prec_16,
						'prec_30' => $prec_30,
						'prec_90' => $prec_90
					];
				
				//$res = $command->insert('agromet','dados',$arrDados,md5(microtime(true)),$options);
			}
			fclose($handle);
		} else {
			
		}
	}
	
	
    public function actionIndex()
    {
    	$fileName = 'precipitacao_acumulada.txt';
    	$fileName = 'dados.csv';
    	$path = \Yii::getAlias('@arquivo/'.$fileName);
    	//$this->processaPrecipitacao($path);
    	$this->processaPrevisao($path);
    }
}
