<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
    	echo \Yii::getAlias('@arquivo');
    }
    
    public function actionImporta()
    {
    	$path = \Yii::getAlias('@arquivo/model_output.csv');
    	$handle = fopen($path, "r");
    	$contador = 0;
    	if ($handle) {
    		while (($line = fgets($handle)) !== false) {
    			echo $line;exit;
    			$contador++;
    			if($contador == 1)continue;
    			$explode = explode(',', $line);
    			
    			$arrMap = [
    				'cod_estacao',
    				'lat',
    				'lon',
    				'datetime',
    				'T_2M',
    				'RELHUM_2m',
    				'TOT_PREC',
    				'CLCT',
    				'V10M',
    				'U10',
    				'V10',
    				'PRESMSL'
    			];
    			
    			var_dump($explode);exit;
    			
    		}
    	
    		fclose($handle);
    	} else {
    		// error opening the file.
    	}
    	
    }
}
