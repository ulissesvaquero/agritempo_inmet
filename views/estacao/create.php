<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Estacao */

$this->title = Yii::t('app', 'Create Estacao');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estacaos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
