<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DadoHistoricoMensal */

$this->title = Yii::t('app', 'Create Dado Historico Mensal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dado Historico Mensals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dado-historico-mensal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
