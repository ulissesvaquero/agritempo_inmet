<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DadoHistoricoMensal;

/**
 * DadoHistoricoMensalSearch represents the model behind the search form about `app\models\DadoHistoricoMensal`.
 */
class DadoHistoricoMensalSearch extends DadoHistoricoMensal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mes', 'ano', 'estacao_id'], 'integer'],
            [['precipitacao'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DadoHistoricoMensal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'precipitacao' => $this->precipitacao,
            'mes' => $this->mes,
            'ano' => $this->ano,
            'estacao_id' => $this->estacao_id,
        ]);

        return $dataProvider;
    }
}
