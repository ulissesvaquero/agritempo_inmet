<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estacao".
 *
 * @property integer $id
 * @property string $codigo
 * @property string $nome
 * @property string $cidade
 * @property string $uf
 * @property double $lat
 * @property double $lon
 * @property double $hp
 * @property integer $is_ativo
 *
 * @property DadoHistoricoMensal[] $dadoHistoricoMensals
 */
class Estacao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estacao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'lon', 'hp'], 'number'],
            [['is_ativo'], 'integer'],
            [['codigo', 'nome', 'cidade'], 'string', 'max' => 200],
            [['uf'], 'string', 'max' => 2],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'nome' => 'Nome',
            'cidade' => 'Cidade',
            'uf' => 'Uf',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'hp' => 'Hp',
            'is_ativo' => 'Is Ativo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDadoHistoricoMensals()
    {
        return $this->hasMany(DadoHistoricoMensal::className(), ['estacao_id' => 'id']);
    }
}
