<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "municipio".
 *
 * @property integer $id
 * @property string $geocode
 * @property string $nome
 * @property string $uf
 * @property double $lat
 * @property double $lon
 */
class Municipio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'municipio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'lon'], 'number'],
            [['geocode'], 'string', 'max' => 45],
            [['nome'], 'string', 'max' => 200],
            [['uf'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'geocode' => 'Geocode',
            'nome' => 'Nome',
            'uf' => 'Uf',
            'lat' => 'Lat',
            'lon' => 'Lon',
        ];
    }
}
