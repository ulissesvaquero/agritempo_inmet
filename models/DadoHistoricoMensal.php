<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dado_historico_mensal".
 *
 * @property integer $id
 * @property double $precipitacao
 * @property integer $mes
 * @property integer $ano
 * @property integer $estacao_id
 *
 * @property Estacao $estacao
 */
class DadoHistoricoMensal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dado_historico_mensal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['precipitacao'], 'number'],
            [['mes', 'ano', 'estacao_id'], 'integer'],
            [['estacao_id'], 'required'],
            [['estacao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estacao::className(), 'targetAttribute' => ['estacao_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'precipitacao' => 'Precipitacao',
            'mes' => 'Mes',
            'ano' => 'Ano',
            'estacao_id' => 'Estacao ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstacao()
    {
        return $this->hasOne(Estacao::className(), ['id' => 'estacao_id']);
    }
}
