<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dados_municipio".
 *
 * @property integer $id
 * @property integer $cod_municipio
 * @property double $lat
 * @property double $lon
 * @property string $data
 * @property double $t_2m
 * @property double $relhum_2m
 * @property double $tot_prec
 * @property double $clct
 * @property double $v10m
 * @property double $u10
 * @property double $v10
 * @property double $presmsl
 */
class DadosMunicipio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dados_municipio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cod_municipio'], 'integer'],
            [['lat', 'lon', 't_2m', 'relhum_2m', 'tot_prec', 'clct', 'v10m', 'u10', 'v10', 'presmsl'], 'number'],
            [['data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_municipio' => 'Cod Municipio',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'data' => 'Data',
            't_2m' => 'T 2m',
            'relhum_2m' => 'Relhum 2m',
            'tot_prec' => 'Tot Prec',
            'clct' => 'Clct',
            'v10m' => 'V10m',
            'u10' => 'U10',
            'v10' => 'V10',
            'presmsl' => 'Presmsl',
        ];
    }
}
